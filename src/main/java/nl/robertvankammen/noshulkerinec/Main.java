package nl.robertvankammen.noshulkerinec;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.Objects;

import static org.bukkit.Material.*;

public class Main extends JavaPlugin implements Listener {
    private static final List<Material> SHULKERSLIST = List.of(SHULKER_BOX, WHITE_SHULKER_BOX, ORANGE_SHULKER_BOX, MAGENTA_SHULKER_BOX, LIGHT_BLUE_SHULKER_BOX, YELLOW_SHULKER_BOX, LIME_SHULKER_BOX, PINK_SHULKER_BOX, GRAY_SHULKER_BOX, LIGHT_GRAY_SHULKER_BOX, CYAN_SHULKER_BOX, PURPLE_SHULKER_BOX, BLUE_SHULKER_BOX, BROWN_SHULKER_BOX, GREEN_SHULKER_BOX, RED_SHULKER_BOX, BLACK_SHULKER_BOX);

    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onClick(InventoryCloseEvent e) {
        if (e.getInventory().getType().equals(InventoryType.ENDER_CHEST)) {
            var location = e.getPlayer().getLocation();
            var messagecheck = false;
            for (ItemStack content : e.getPlayer().getEnderChest().getContents()) {
                if (content != null && SHULKERSLIST.contains(content.getType())) {
                    Objects.requireNonNull(location.getWorld()).dropItem(location, content);
                    e.getPlayer().getEnderChest().remove(content);
                    messagecheck = true;
                }
            }
            if (messagecheck){
                e.getPlayer().sendMessage("Shulkerboxes are not allowed in a encherchest");
            }
        }
        System.out.println(e.getInventory().getType());
    }

}
